﻿using UserSettingsModule.Models;

namespace UserSettingsModule.IServices
{
    public interface ISaveSettingsService
    {
        /// <summary>
        /// Saves the <paramref name="settings" /> of instance <typeparamref name="TSettings" /> to a filename defined as json.
        /// </summary>
        /// <typeparam name="TSettings">The instance type.</typeparam>
        /// <param name="settings">The instance of type <typeparamref name="TSettings" />.</param>
        /// <returns>True if successful. Otherwise, false.</returns>
        bool Save<TSettings>(TSettings settings) where TSettings : SettingsBase;
    }
}