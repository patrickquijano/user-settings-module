﻿using UserSettingsModule.Models;

namespace UserSettingsModule.IServices
{
    public interface IReadSettingsService
    {
        /// <summary>
        /// Reads the json from a filename defined and converts it to an instance of type <typeparamref name="TSettings" />.
        /// </summary>
        /// <typeparam name="TSettings">The instance type.</typeparam>
        /// <returns>The instance with type <typeparamref name="TSettings" />.</returns>
        TSettings Read<TSettings>() where TSettings : SettingsBase;
    }
}