﻿using System;

namespace UserSettingsModule.Extensions
{
    internal static class ExceptionExtension
    {
        public static Exception GetInnerException(this Exception input)
        {
            return input.InnerException != null ? input.GetInnerException() : input;
        }
    }
}
