﻿using System;

namespace UserSettingsModule.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Sensitive : Attribute
    {
    }
}
