﻿namespace UserSettingsModule.IRepositories
{
    public interface IReadJsonRepository
    {
        /// <summary>
        /// Reads the json in the provided file path <paramref name="filename" /> and converts it to an instance with type <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The instance type.</typeparam>
        /// <param name="filename">The json file path.</param>
        /// <returns>The instance <typeparamref name="T" /> converted from json.</returns>
        /// <returns></returns>
        T Read<T>(string filename);
    }
}