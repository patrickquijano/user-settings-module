﻿namespace UserSettingsModule.IRepositories
{
    public interface IWriteJsonRepository
    {
        /// <summary>
        /// Writes the instance of type <typeparamref name="T" /> to a <paramref name="filename" /> as json.
        /// </summary>
        /// <typeparam name="T">The object type.</typeparam>
        /// <param name="model">The instance of <typeparamref name="T" />.</param>
        /// <param name="filename">The output filename.</param>
        /// <returns>True if successful. Otherwise, false.</returns>
        bool Write<T>(T model, string filename);
    }
}