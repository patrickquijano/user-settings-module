﻿using Newtonsoft.Json;
using System;
using System.IO;
using UserSettingsModule.Extensions;
using UserSettingsModule.IRepositories;

namespace UserSettingsModule.Repositories
{
    internal class ReadJsonRepository : IReadJsonRepository
    {
        /// <summary>
        /// Reads the json in the provided file path <paramref name="filename" /> and converts it to instance with type <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The instance type.</typeparam>
        /// <param name="filename">The json file path.</param>
        /// <returns>The instance <typeparamref name="T" /> converted from json.</returns>
        /// <exception cref="ArgumentNullException">The <paramref name="filename" /> is empty or null.</exception>
        /// <exception cref="FileNotFoundException">The <paramref name="filename" /> does not exist.</exception>
        public T Read<T>(string filename)
        {
            if (filename.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException($"The argument `{nameof(filename)}` of type {filename.GetType()} is either a whitespace or a null.");
            }
            if (!File.Exists(filename))
            {
                throw new FileNotFoundException($"The file `{filename}` does not exist.");
            }
            var json = File.ReadAllText(filename);
            var obj = JsonConvert.DeserializeObject<T>(json);

            return obj;
        }
    }
}
