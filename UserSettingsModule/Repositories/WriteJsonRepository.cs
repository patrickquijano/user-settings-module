﻿using Newtonsoft.Json;
using System;
using System.IO;
using UserSettingsModule.Extensions;
using UserSettingsModule.IRepositories;

namespace UserSettingsModule.Repositories
{
    internal class WriteJsonRepository : IWriteJsonRepository
    {
        /// <summary>
        /// Writes the instance of type <typeparamref name="T" /> to a <paramref name="filename" /> as json.
        /// </summary>
        /// <typeparam name="T">The object type.</typeparam>
        /// <param name="model">The instance of <typeparamref name="T" />.</param>
        /// <param name="filename">The output filename.</param>
        /// <returns>True if successful. Otherwise, false.</returns>
        /// <exception cref="ArgumentNullException">The <paramref name="model" /> is null or <paramref name="filename"/> is null or whitespace.</exception>
        public bool Write<T>(T model, string filename)
        {
            if (model == null)
            {
                throw new ArgumentNullException($"The argument `{nameof(model)}` of type {model.GetType()} is null.");
            }
            if (filename.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException($"The argument `{nameof(filename)}` of type {filename.GetType()} is either a whitespace or a null.");
            }
            var directory = Path.GetDirectoryName(filename);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            var json = JsonConvert.SerializeObject(model, Formatting.Indented);
            File.WriteAllText(filename, json);

            return true;
        }
    }
}
