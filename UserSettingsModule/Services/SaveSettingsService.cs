﻿using System;
using System.Diagnostics;
using System.Reflection;
using UserSettingsModule.Attributes;
using UserSettingsModule.Extensions;
using UserSettingsModule.IRepositories;
using UserSettingsModule.IServices;
using UserSettingsModule.Models;

namespace UserSettingsModule.Services
{
    internal class SaveSettingsService : ISaveSettingsService
    {
        private readonly string _PassPhrase;
        private readonly string _SettingsFile;
        private readonly IWriteJsonRepository _WriteJsonRepository;

        public SaveSettingsService(string passPhrase,
                                   string settingsFile,
                                   IWriteJsonRepository writeJsonRepository)
        {
            this._PassPhrase = passPhrase;
            this._SettingsFile = settingsFile;
            this._WriteJsonRepository = writeJsonRepository;
        }

        /// <summary>
        /// Saves the <paramref name="settings" /> of instance <typeparamref name="TSettings" /> to a filename defined as json.
        /// </summary>
        /// <typeparam name="TSettings">The instance type.</typeparam>
        /// <param name="settings">The instance of type <typeparamref name="TSettings" />.</param>
        /// <returns>True if successful. Otherwise, false.</returns>
        public bool Save<TSettings>(TSettings settings) where TSettings : SettingsBase
        {
            try
            {
                this.EncryptSensitiveProperties(settings);
                var result = this._WriteJsonRepository.Write(settings, this._SettingsFile);

                return result;
            }
            catch (ArgumentNullException exception)
            {
                Debug.WriteLine("Error encountered while saving settings.");
                Debug.WriteLine(exception.GetInnerException());

                return false;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Error encountered while saving settings.");
                Debug.WriteLine(exception.GetInnerException());

                return false;
            }
        }

        /// <summary>
        /// Encrypts properties of <paramref name="settings" /> of instance type <typeparamref name="TSettings" /> with Sensitive attribute.
        /// </summary>
        /// <typeparam name="TSettings">The instance type.</typeparam>
        /// <param name="settings">The instance of type <typeparamref name="TSettings" />.</param>
        private void EncryptSensitiveProperties<TSettings>(TSettings settings) where TSettings : SettingsBase
        {
            var propertyInfos = settings.GetType().GetProperties();
            if (!this._PassPhrase.IsNullOrWhiteSpace())
            {
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    if (propertyInfo.PropertyType != typeof(string) || !Attribute.IsDefined(propertyInfo, typeof(Sensitive)))
                    {
                        continue;
                    }
                    var value = propertyInfo.GetValue(settings).ToString();
                    propertyInfo.SetValue(settings, value.Encrypt(this._PassPhrase));
                }
            }
        }
    }
}
