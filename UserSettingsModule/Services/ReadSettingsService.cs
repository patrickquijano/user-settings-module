﻿using System;
using System.Reflection;
using UserSettingsModule.Attributes;
using UserSettingsModule.Extensions;
using UserSettingsModule.IRepositories;
using UserSettingsModule.IServices;
using UserSettingsModule.Models;

namespace UserSettingsModule.Services
{
    internal class ReadSettingsService : IReadSettingsService
    {
        private readonly string _PassPhrase;
        private readonly string _SettingsFile;
        private readonly IReadJsonRepository _ReadJsonRepository;

        public ReadSettingsService(string passPhrase,
                                   string settingsFile,
                                   IReadJsonRepository readJsonRepository)
        {
            this._PassPhrase = passPhrase;
            this._SettingsFile = settingsFile;
            this._ReadJsonRepository = readJsonRepository;
        }

        /// <summary>
        /// Reads the json from a filename defined and converts it to an instance of type <typeparamref name="TSettings" />.
        /// </summary>
        /// <typeparam name="TSettings">The instance type.</typeparam>
        /// <returns>The instance with type <typeparamref name="TSettings" />.</returns>
        public TSettings Read<TSettings>() where TSettings : SettingsBase
        {
            try
            {
                var settings = this._ReadJsonRepository.Read<TSettings>(this._SettingsFile);
                this.DecrpytSensitiveProperties(settings);

                return settings;
            }
            catch
            {
                return (TSettings)Activator.CreateInstance(typeof(TSettings));
            }
        }

        /// <summary>
        /// Decrypts properties of <paramref name="settings" /> of instance type <typeparamref name="TSettings" /> with Sensitive attribute.
        /// </summary>
        /// <typeparam name="TSettings">The instance type.</typeparam>
        /// <param name="settings">The instance of type <typeparamref name="TSettings" />.</param>
        private void DecrpytSensitiveProperties<TSettings>(TSettings settings) where TSettings : SettingsBase
        {
            var propertyInfos = settings.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (propertyInfo.PropertyType != typeof(string) || !Attribute.IsDefined(propertyInfo, typeof(Sensitive)) || this._PassPhrase.IsNullOrWhiteSpace())
                {
                    continue;
                }
                var value = propertyInfo.GetValue(settings).ToString();
                propertyInfo.SetValue(settings, value.Decrypt(this._PassPhrase));
            }
        }
    }
}
