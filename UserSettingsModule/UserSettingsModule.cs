﻿using Autofac;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UserSettingsModule.IRepositories;
using UserSettingsModule.IServices;
using UserSettingsModule.Repositories;
using UserSettingsModule.Services;

namespace UserSettingsModule
{
    public class UserSettingsModule : Autofac.Module
    {
        public Assembly Assembly => Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
        /// <summary>
        /// The pass phrase used to encrypt/decypt a string property in the Settings model.
        /// </summary>
        public string PassPhrase { get; set; }
        /// <summary>
        /// The path to the settings file.
        /// </summary>
        public string SettingsFile { get; set; }

        public UserSettingsModule()
        {
            this.PassPhrase = $"{Environment.MachineName}{Environment.UserDomainName}{Environment.UserName}";
            this.SettingsFile = Debugger.IsAttached ? Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), this.Assembly.GetName().Name, "settings.json") : Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), this.Assembly.GetName().Name, "settings.json");
        }

        protected override void Load(ContainerBuilder builder)
        {
            // Register repositories
            builder.RegisterType<ReadJsonRepository>().As<IReadJsonRepository>();
            builder.RegisterType<WriteJsonRepository>().As<IWriteJsonRepository>();

            // Register services
            builder.RegisterType<ReadSettingsService>().As<IReadSettingsService>()
                   .WithParameter("passPhrase", this.PassPhrase)
                   .WithParameter("settingsFile", this.SettingsFile);
            builder.RegisterType<SaveSettingsService>().As<ISaveSettingsService>()
                   .WithParameter("passPhrase", this.PassPhrase)
                   .WithParameter("settingsFile", this.SettingsFile);
        }
    }
}
