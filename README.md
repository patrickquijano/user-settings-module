# User Settings Module

An Autofac module that saves user settings to a json file.

## Usage

Create a model that implements `UserSettingsModule.Models.SettingsBase`. Use the `UserSettingsModule.Attributes.Sensitive` attribute if the property is sensitive like passwords.

```C#
using UserSettingsModule.Attributes;
using UserSettingsModule.Models;
	
namespace SampleNamespace
{
	public class Settings : SettingsBase
	{
		public string Name { get; set; }
		[Sensitive]
		public string Password { get; set; }
	}
}
```

Register the module in the Container.

```C#
var builder = new ContainerBuilder();
builder.RegisterModule<UserSettingsModule.UserSettingsModule>();
```

Implement the services `UserSettingsModule.IServices.IReadSettingsService` and `UserSettingsModule.IServices.ISaveSettingsService`.

```C#
using UserSettingsModule.IServices;
// other imports
		
namespace SampleNamespace
{
	public class SampleClass
	{
		private readonly IReadSettingsService _ReadSettingsService;
		private readonly ISaveSettingsService _SaveSettingsService;
				
		public SampleClass(IReadSettingsService readSettingsService, ISaveSettingsService saveSettingsService)
		{
			this._ReadSettingsService = readSettingsService;
			this._SaveSettingsService = saveSettingsService;
			// other implementations
		}
				
		public void SampleMethod()
		{
			var settings = this._ReadSettingsService.Read<Settings>();
			settings.Name = "John Doe";
			settings.Password = "Test Password";
			this._SaveSettings.Save();
		}
	}
}
```
