﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using UserSettingsModule.Attributes;
using UserSettingsModule.IServices;
using UserSettingsModule.Models;

namespace UserSettingsModuleTests.Services
{
    [TestClass]
    public class SettingsServiceTest : TestBase
    {
        private IReadSettingsService _ReadSettingsService;
        private ISaveSettingsService _SaveSettingsService;

        [TestInitialize]
        public void Setup()
        {
            this._ReadSettingsService = this.Resolve<IReadSettingsService>();
            this._SaveSettingsService = this.Resolve<ISaveSettingsService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void SaveAndRead()
        {
            var settings = new Settings
            {
                TestNumber = 123456,
                TestString = "Test String",
            };
            var saved = this._SaveSettingsService.Save(settings);
            Assert.IsTrue(saved);

            var results = this._ReadSettingsService.Read<Settings>();
            Debug.WriteLine(results.TestString);
            Assert.IsNotNull(results);
        }
    }

    internal class Settings : SettingsBase
    {
        [Sensitive]
        public string TestString { get; set; }
        public int TestNumber { get; set; }
    }
}
